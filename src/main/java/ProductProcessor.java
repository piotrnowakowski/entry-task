public interface ProductProcessor {
    void processProduct(SummerClothingProduct product);
    void showResults();
}
