import com.opencsv.bean.CsvBindByName;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class SummerClothingProduct {
    @CsvBindByName(column = "title", required = true)
    private String title;
    @CsvBindByName(column = "origin_country")
    private String countryOfOrigin;
    @CsvBindByName(column = "price", required = true)
    private Double price;
    @CsvBindByName(column = "rating_count")
    private int ratingCount;
    @CsvBindByName(column = "rating_five_count")
    private int ratingFiveCount;
}
