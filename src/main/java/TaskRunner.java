import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import com.opencsv.bean.CsvToBeanBuilder;

public class TaskRunner {
    List<ProductProcessor> productProcessors = new ArrayList<>();

    public static void main(String[] args) {
        TaskRunner task = new TaskRunner();
        try {
            task.run();
        } catch (URISyntaxException | FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    private void run() throws URISyntaxException, FileNotFoundException {
        productProcessors.add(new AverageProductPricePerCountryProductProcessor());
        productProcessors.add(new FiveStarProductSharePerCountryProductProcessor());
        File csvFile = getCsvFileFromResource("test-task_dataset_summer_products.csv");
        List<SummerClothingProduct> products = new CsvToBeanBuilder(new FileReader(csvFile))
                                   .withType(SummerClothingProduct.class).build().parse();
        products.forEach(this::processProduct);
        showProcessorsResults();
    }

    private void showProcessorsResults() {
        productProcessors.forEach(ProductProcessor::showResults);
    }

    private void processProduct(SummerClothingProduct product) {
//        System.out.println("Processing product: " + product.getTitle());
        productProcessors.forEach(productProcessor -> productProcessor.processProduct(product));
    }

    private File getCsvFileFromResource(String fileName) throws URISyntaxException {
        URL resource = getClass().getClassLoader().getResource(fileName);
        if (resource == null) {
            throw new IllegalArgumentException("File not found: " + fileName);
        } else {
            return new File(resource.toURI());
        }
    }

}
