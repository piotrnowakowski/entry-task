import java.util.HashMap;
import java.util.Map;

public class FiveStarProductSharePerCountryProductProcessor implements ProductProcessor {

    private Map<String, Integer> countryTofiveStarRatedProcuctsMap = new HashMap<>();
    private Map<String, Integer> countryToAllProductsCount = new HashMap<>();

    @Override
    public void processProduct(SummerClothingProduct product) {
        if (productIsValid(product)) {

            Integer fiveStarRatingsForCountry = countryTofiveStarRatedProcuctsMap
                                          .getOrDefault(product.getCountryOfOrigin(), 0);
            Integer allRatesPercountry = countryToAllProductsCount
                                                .getOrDefault(product.getCountryOfOrigin(), 0);
            fiveStarRatingsForCountry += product.getRatingFiveCount();
            allRatesPercountry+= product.getRatingCount();
            countryTofiveStarRatedProcuctsMap.put(product.getCountryOfOrigin(), fiveStarRatingsForCountry);
            countryToAllProductsCount.put(product.getCountryOfOrigin(), allRatesPercountry);
        }  else {
        System.err
            .println("Product invalid to be included in five star product share per country processor. " + product.toString());
    }

    }

    @Override
    public void showResults() {
        System.out.println("Share of 5 rated products per country: ");
        countryTofiveStarRatedProcuctsMap.forEach((countryCode, fiveRatedCount) -> {
            Integer allRatesByContry = countryToAllProductsCount.get(countryCode);

            double rateFiveShare  = ((double)fiveRatedCount / allRatesByContry) * 100;
            System.out.println(
                countryCode + ": " + rateFiveShare + " (from " + fiveRatedCount + " 5 star rate(s) and total "
                + "rates count of + " + allRatesByContry + ")");
        });

    }

    private boolean productIsValid(SummerClothingProduct product) {
        return product.getCountryOfOrigin() != null && !"".equals(product.getCountryOfOrigin());
    }
}
