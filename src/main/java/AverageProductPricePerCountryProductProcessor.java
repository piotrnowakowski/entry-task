import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang3.tuple.Pair;

public class AverageProductPricePerCountryProductProcessor implements ProductProcessor {

    Map<String, Pair<Integer, Double>> countryToSummaryProductCostsMap = new HashMap<>();

    @Override
    public void processProduct(SummerClothingProduct product) {
        if (productIsValid(product)) {
            Pair<Integer, Double> dataForCountry = countryToSummaryProductCostsMap
                                                       .getOrDefault(product.getCountryOfOrigin(),
                                                                     Pair.of(0, 0d));
            int productCount = dataForCountry.getLeft();
            double summaryCost = dataForCountry.getRight() + product.getPrice();
            countryToSummaryProductCostsMap.put(product.getCountryOfOrigin(), Pair.of(++productCount, summaryCost));
        } else {
            System.err
                .println("Product invalid to be included in average cost per country processor. " + product.toString());
        }
    }

    private boolean productIsValid(SummerClothingProduct product) {
        return product.getPrice() != null && product.getCountryOfOrigin() != null && !"".equals(
            product.getCountryOfOrigin());
    }

    @Override
    public void showResults() {
        System.out.println("Average costs per country: ");
        countryToSummaryProductCostsMap.forEach((countryCode, summaryData) -> {
            double averageCost = summaryData.getRight() / summaryData.getLeft();
            System.out.println(
                countryCode + ": " + averageCost + " (from " + summaryData.getLeft() + " item(s) with total value of + "
                + summaryData.getRight() + ")");
        });
    }

}
